#!/bin/bash

# Build the EPICS module
# Override PROJECT and LIBVERSION
# - PROJECT can't be guessed from the working directory
# - If we apply patches, the version will be set to the username
make PROJECT=pvDatabaseCPP LIBVERSION=4.2.0
make PROJECT=pvDatabaseCPP LIBVERSION=4.2.0 install

# Clean builddir between variants builds
rm -rf builddir
