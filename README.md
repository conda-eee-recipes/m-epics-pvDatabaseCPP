m-epics-pvDatabaseCPP conda recipe
==================================

Home: https://bitbucket.org/europeanspallationsource/m-epics-pvDatabaseCPP

Package license: EPICS Open License

Recipe license: BSD 2-Clause

Summary: EPICS pvDatabaseCPP module
